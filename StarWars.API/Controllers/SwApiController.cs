using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarWars.Service.Services.Interfaces;

namespace StarWars.API.Controllers;

[Route("api/swapi")]
[ApiController]
public class SwApiController : ControllerBase
{
    private readonly ILogger<SwApiController> _logger;

    private readonly ISwApiLoaderService _swApiLoaderService;

    // GET
    public SwApiController(ISwApiLoaderService swApiLoaderService, ILogger<SwApiController> logger)
    {
        _swApiLoaderService = swApiLoaderService;
        _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> Load()
    {
        try
        {
            await _swApiLoaderService.Load();
            return Ok(new
            {
                Result = true
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500);
        }
    }
}