﻿using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System.Linq.Dynamic.Core;
using MediatR;
using Newtonsoft.Json;
using StarWars.Data.Entities;
using StarWars.Domain.DTOs;
using StarWars.Service.Queries;
using StarWars.Service.Services.Interfaces;

namespace StarWars.API.Controllers;

[Route("api/films")]
[ApiController]
// [ResponseCache(CacheProfileName = "Default30")]
public class FilmsController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly ILogger<FilmsController> _logger;

    public FilmsController(ILogger<FilmsController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<IActionResult> Get(string? search, string? populate, string? orderBy)
    {
        try
        {
            return Ok(new ResponseDto<List<Film>>
            {
                Payload = await _mediator.Send(new GetFilmsQuery
                {
                    Populate = populate,
                    OrderBy = orderBy,
                    SearchExp = search,
                    CacheKey = $"{Request.Path}{Request.QueryString}"
                })
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500, new ResponseDto<List<Film>>
            {
                Errors = new List<string>
                {
                    "An error occurred getting the films. Please try again in a few minutes.",
                    e.Message
                }
            });
        }
    }

    // public IActionResult Privacy()
    // {
    //     return View();
    // }
    //
    // [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    // public IActionResult Error()
    // {
    //     return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    // }
}