using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarWars.Data.Entities;
using StarWars.Domain.DTOs;
using StarWars.Service.Queries;

namespace StarWars.API.Controllers;

[Route("api/people")]
[ApiController]
// [ResponseCache(CacheProfileName = "Default30")]
public class PeopleController : ControllerBase
{
    private readonly ILogger<PeopleController> _logger;
    private readonly IMediator _mediator;

    public PeopleController(ILogger<PeopleController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<IActionResult> Get(string? search, string? populate, string? orderBy)
    {
        try
        {
            return Ok(new ResponseDto<List<People>>
            {
                Payload = await _mediator.Send(new GetPeopleQuery
                {
                    Populate = populate,
                    OrderBy = orderBy,
                    SearchExp = search,
                    CacheKey = $"{Request.Path}{Request.QueryString}"
                })
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500, new ResponseDto<List<Film>>
            {
                Errors = new List<string>
                {
                    "An error occurred getting the people. Please try again in a few minutes."
                }
            });
        }
    }
}