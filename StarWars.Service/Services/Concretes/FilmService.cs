using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Domain.DTOs;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services.Concretes;

public class FilmService : IFilmService
{
    private readonly IRepository<Film> _filmRepository;

    private readonly IStarWarsSwApiService _starWarsSwApiService;
    private readonly IPopulateService _populateService;
    private readonly IMapper _mapper;

    public FilmService(IRepository<Film> filmRepository,IStarWarsSwApiService starWarsSwApiService,
        IPopulateService populateService, IMapper mapper)
    {
        _filmRepository = filmRepository;
        _starWarsSwApiService = starWarsSwApiService;
        _populateService = populateService;
        _mapper = mapper;
    }

    public IRepository<Film> Repository()
    {
        return _filmRepository;
    }

    public Task<Film> AddAsync(Film entity)
    {
        return _filmRepository.AddAsync(entity);
    }

    public Task Clear<TX>()
    {
        throw new NotImplementedException();
    }

    public async Task Clear()
    {
        await _filmRepository.Clear();
    }
    
    private List<string> GetPopulatePropertyNames<T>(string populate)
    {
        var populateProps = new List<string>();
        var propertyInfos = typeof(T)
            .GetProperties(BindingFlags.Public | BindingFlags.Instance);
        var propToPopulate = populate.Trim().Split(',');
        foreach (var propertyFromQueryName in propToPopulate.Where(p => !string.IsNullOrWhiteSpace(p)))
        {
            var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
            if (objectProperty == null || objectProperty.GetType() == typeof(ICollection<T>))
                continue;
            populateProps.Add(objectProperty.Name);
        }

        return populateProps;
    }

    public async Task<List<Film>> GetFromSwApi(string? search, string? populate, string? orderBy)
    {
        var films = new List<Film>();
        
        var responseDto = await _starWarsSwApiService.DoRequestMany<FilmDto>("films", 1);
        
        if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any()) return films;

        var populateProps = new List<string>();
        if (!string.IsNullOrEmpty(populate))
        {
            populateProps = GetPopulatePropertyNames<Film>(populate);
        }
        
        foreach (var filmDto in responseDto.Results)
        {
            var film = _mapper.Map<FilmDto, Film>(filmDto);
            if (populate == null)
            {
                films.Add(film);
                continue;
            }

            if (populateProps.Contains("Characters"))
            {
                film.Characters = await _populateService.PopulateMany<PeopleDto, People>(filmDto.Characters);
            }
            if (populateProps.Contains("Planets"))
            {
                film.Planets = await _populateService.PopulateMany<PlanetDto, Planet>(filmDto.Planets);
            }
            if (populateProps.Contains("Starships"))
            {
                film.Starships = await _populateService.PopulateMany<StarshipDto, Starship>(filmDto.Starships);
            }
            if (populateProps.Contains("Vehicles"))
            {
                film.Vehicles = await _populateService.PopulateMany<VehicleDto, Vehicle>(filmDto.Vehicles);
            }
            if (populateProps.Contains("Species"))
            {
                film.Species = await _populateService.PopulateMany<SpecieDto, Specie>(filmDto.Species);
            }
            films.Add(film);
        }

        var result = (responseDto.Count != null ? (decimal)responseDto.Count : 0) / 10;
        var totalOfPages = (int)Math.Ceiling(result);
        if (result % 1 != 0) totalOfPages++;
        for (var i = 2; i <= totalOfPages; i++)
        {
            responseDto = await _starWarsSwApiService.DoRequestMany<FilmDto>("films", i);
        
            if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any()) return films;
        
            foreach (var filmDto in responseDto.Results)
            {
                var film = _mapper.Map<FilmDto, Film>(filmDto);
                if (populate == null)
                {
                    films.Add(film);
                    continue;
                }

                if (populateProps.Contains("Characters"))
                {
                    film.Characters = await _populateService.PopulateMany<PeopleDto, People>(filmDto.Characters);
                }
                if (populateProps.Contains("Planets"))
                {
                    film.Planets = await _populateService.PopulateMany<PlanetDto, Planet>(filmDto.Planets);
                }
                if (populateProps.Contains("Starships"))
                {
                    film.Starships = await _populateService.PopulateMany<StarshipDto, Starship>(filmDto.Starships);
                }
                if (populateProps.Contains("Vehicles"))
                {
                    film.Vehicles = await _populateService.PopulateMany<VehicleDto, Vehicle>(filmDto.Vehicles);
                }
                if (populateProps.Contains("Species"))
                {
                    film.Species = await _populateService.PopulateMany<SpecieDto, Specie>(filmDto.Species);
                }
                films.Add(film);
            }
        }
        
        if (!string.IsNullOrEmpty(search))
        {
            films = films.Where(f => 
                (f.Director != null ? f.Director.ToLower().Contains(search) : true) ||
                (f.Producer != null ? f.Producer.ToLower().Contains(search) : true) ||
                (f.Title != null ? f.Title.ToLower().Contains(search) : true) ||
                (f.Url != null ? f.Url.ToLower().Contains(search) : true) ||
                (f.EpisodeId != null ? f.EpisodeId.ToLower().Contains(search) : true) ||
                (f.OpeningCrawl != null ? f.OpeningCrawl.ToLower().Contains(search) : true) ||
                f.Id.ToString().Contains(search)
            ).ToList();
        }
        
        if (string.IsNullOrEmpty(orderBy)) return films;
        {
            var propertyInfos = typeof(People)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var orderQueryBuilder = new StringBuilder();
            var orderParams = orderBy.Trim().Split(',');
            foreach (var param in orderParams.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var propertyFromQueryName = param.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.EndsWith(" desc") ? "DESC" : "ASC";
                orderQueryBuilder.Append($"{objectProperty.Name} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            films = films.AsQueryable().OrderBy(orderQuery).ToList();
        }
        
        return films;
    }
    
    public Task<List<Film>> Get(string? search, string? populate, string? orderBy)
    {
        var propertyInfos = typeof(Film)
            .GetProperties(BindingFlags.Public | BindingFlags.Instance);

        var query = _filmRepository.Query().AsQueryable();

        if (!string.IsNullOrEmpty(populate))
        {
            query = query.AsNoTracking();
            var propToPopulate = populate.Trim().Split(',');
            foreach (var propertyFromQueryName in propToPopulate.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                query = query.Include(objectProperty.Name);
            }
        }

        if (!string.IsNullOrEmpty(search))
        {
            query = query.Where(f => 
                (f.Director != null ? f.Director.ToLower().Contains(search) : true) ||
                (f.Producer != null ? f.Producer.ToLower().Contains(search) : true) ||
                (f.Title != null ? f.Title.ToLower().Contains(search) : true) ||
                (f.Url != null ? f.Url.ToLower().Contains(search) : true) ||
                (f.EpisodeId != null ? f.EpisodeId.ToLower().Contains(search) : true) ||
                (f.OpeningCrawl != null ? f.OpeningCrawl.ToLower().Contains(search) : true) ||
                f.Id.ToString().Contains(search)
            );
        }
        
        if (string.IsNullOrEmpty(orderBy)) return query.ToListAsync();
        {
            var orderQueryBuilder = new StringBuilder();
            var orderParams = orderBy.Trim().Split(',');
            foreach (var param in orderParams.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var propertyFromQueryName = param.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.EndsWith(" desc") ? "DESC" : "ASC";
                orderQueryBuilder.Append($"{objectProperty.Name} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            query = query.OrderBy(orderQuery);
        }

        return query.ToListAsync();
    }
}