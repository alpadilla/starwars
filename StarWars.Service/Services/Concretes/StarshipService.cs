using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class StarshipService : IStarshipService
{
    private readonly IRepository<Starship> _starshipRepository;

    public StarshipService(IRepository<Starship> starshipRepository)
    {
        _starshipRepository = starshipRepository;
    }

    public IRepository<Starship> Repository()
    {
        return _starshipRepository;
    }

    public async Task<Starship> AddAsync(Starship entity)
    {
        return await _starshipRepository.AddAsync(entity);
    }

    public async Task Clear()
    {
        await _starshipRepository.Clear();
    }
}