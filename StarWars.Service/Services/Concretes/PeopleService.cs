using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Domain.DTOs;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services.Concretes;

public class PeopleService : IPeopleService
{
    private readonly IRepository<People> _peopleRepository;
    
    private readonly IPopulateService _populateService;
    private readonly IStarWarsSwApiService _starWarsSwApiService;
    private readonly IMapper _mapper;

    public PeopleService(IStarWarsSwApiService starWarsSwApiService, IMapper mapper,
        IPopulateService populateService, IRepository<People> peopleRepository)
    {
        _starWarsSwApiService = starWarsSwApiService;
        _mapper = mapper;
        _populateService = populateService;
        _peopleRepository = peopleRepository;
    }

    private List<string> GetPopulatePropertyNames<T>(string populate)
    {
        var populateProps = new List<string>();
        var propertyInfos = typeof(T)
            .GetProperties(BindingFlags.Public | BindingFlags.Instance);
        var propToPopulate = populate.Trim().Split(',');
        foreach (var propertyFromQueryName in propToPopulate.Where(p => !string.IsNullOrWhiteSpace(p)))
        {
            var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
            if (objectProperty == null || objectProperty.GetType() == typeof(ICollection<T>))
                continue;
            populateProps.Add(objectProperty.Name);
        }

        return populateProps;
    }

    public async Task<List<People>> GetFromSwApi(string? search, string? populate, string? orderBy)
    {
        var people = new List<People>();
        
        var responseDto = await _starWarsSwApiService.DoRequestMany<PeopleDto>("people", 1);
        
        if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any()) return people;

        var populateProps = new List<string>();
        if (!string.IsNullOrEmpty(populate))
        {
            populateProps = GetPopulatePropertyNames<People>(populate);
        }
        
        foreach (var peopleDto in responseDto.Results)
        {
            var newPeople = _mapper.Map<PeopleDto, People>(peopleDto);
            if (populate == null)
            {
                people.Add(newPeople);
                continue;
            }

            if (populateProps.Contains("Films"))
            {
                newPeople.Films = await _populateService.PopulateMany<FilmDto, Film>(peopleDto.Films);
            }

            if (populateProps.Contains("Species"))
            {
                newPeople.Species = await _populateService.PopulateMany<SpecieDto, Specie>(peopleDto.Species);
            }

            if (populateProps.Contains("Vehicles"))
            {
                newPeople.Vehicles = await _populateService.PopulateMany<VehicleDto, Vehicle>(peopleDto.Vehicles);
            }

            if (populateProps.Contains("Starships"))
            {
                newPeople.Starships = await _populateService.PopulateMany<StarshipDto, Starship>(peopleDto.Starships);
            }
            
            people.Add(newPeople);
        }
        
        var result = (responseDto.Count != null ? (decimal)responseDto.Count : 0) / 10;
        var totalOfPages = (int)Math.Ceiling(result);
        if (result % 1 != 0) totalOfPages++;
        for (var i = 2; i <= totalOfPages; i++)
        {
            responseDto = await _starWarsSwApiService.DoRequestMany<PeopleDto>("people", i);
        
            if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any()) return people;
        
            foreach (var peopleDto in responseDto.Results)
            {
                var newPeople = _mapper.Map<PeopleDto, People>(peopleDto);
                if (populateProps.Contains("Films"))
                {
                    newPeople.Films = await _populateService.PopulateMany<FilmDto, Film>(peopleDto.Films);
                }

                if (populateProps.Contains("Species"))
                {
                    newPeople.Species = await _populateService.PopulateMany<SpecieDto, Specie>(peopleDto.Species);
                }

                if (populateProps.Contains("Vehicles"))
                {
                    newPeople.Vehicles = await _populateService.PopulateMany<VehicleDto, Vehicle>(peopleDto.Vehicles);
                }

                if (populateProps.Contains("Starships"))
                {
                    newPeople.Starships = await _populateService.PopulateMany<StarshipDto, Starship>(peopleDto.Starships);
                }
                
                people.Add(newPeople);
            }
        }
        
        if (!string.IsNullOrEmpty(search))
        {
            people = people.Where(f => 
                (f.Name != null ? f.Name.ToLower().Contains(search) : true) ||
                (f.Height != null ? f.Height.ToLower().Contains(search) : true) ||
                (f.Mass != null ? f.Mass.ToLower().Contains(search) : true) ||
                (f.Url != null ? f.Url.ToLower().Contains(search) : true) ||
                (f.HairColor != null ? f.HairColor.ToLower().Contains(search) : true) ||
                (f.SkinColor != null ? f.SkinColor.ToLower().Contains(search) : true) ||
                (f.EyeColor != null ? f.EyeColor.ToLower().Contains(search) : true) ||
                (f.BirthYear != null ? f.BirthYear.ToLower().Contains(search) : true) ||
                (f.Gender != null ? f.Gender.ToLower().Contains(search) : true) ||
                (f.Homeworld != null ? f.Homeworld.ToLower().Contains(search) : true) ||
                (f.Planet != null && f.Planet.Name != null ? f.Planet.Name.ToLower().Contains(search) : true) ||
                f.Id.ToString().Contains(search)
            ).ToList();
        }
        
        if (string.IsNullOrEmpty(orderBy)) return people;
        {
            var propertyInfos = typeof(People)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var orderQueryBuilder = new StringBuilder();
            var orderParams = orderBy.Trim().Split(',');
            foreach (var param in orderParams.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var propertyFromQueryName = param.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.EndsWith(" desc") ? "DESC" : "ASC";
                orderQueryBuilder.Append($"{objectProperty.Name} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            people = people.AsQueryable().OrderBy(orderQuery).ToList();
        }
        
        return people;
    }

    public Task<List<People>> Get(string? search, string? populate, string? orderBy)
    {
        var propertyInfos = typeof(People)
            .GetProperties(BindingFlags.Public | BindingFlags.Instance);

        var query = _peopleRepository.Query().AsQueryable();

        if (!string.IsNullOrEmpty(populate))
        {
            query = query.AsNoTracking();
            var propToPopulate = populate.Trim().Split(',');
            foreach (var propertyFromQueryName in propToPopulate.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                query = query.Include(objectProperty.Name);
            }
        }

        if (!string.IsNullOrEmpty(search))
        {
            query = query.Where(f => 
                (f.Name != null ? f.Name.ToLower().Contains(search) : true) ||
                (f.Height != null ? f.Height.ToLower().Contains(search) : true) ||
                (f.Mass != null ? f.Mass.ToLower().Contains(search) : true) ||
                (f.Url != null ? f.Url.ToLower().Contains(search) : true) ||
                (f.HairColor != null ? f.HairColor.ToLower().Contains(search) : true) ||
                (f.SkinColor != null ? f.SkinColor.ToLower().Contains(search) : true) ||
                (f.EyeColor != null ? f.EyeColor.ToLower().Contains(search) : true) ||
                (f.BirthYear != null ? f.BirthYear.ToLower().Contains(search) : true) ||
                (f.Gender != null ? f.Gender.ToLower().Contains(search) : true) ||
                (f.Homeworld != null ? f.Homeworld.ToLower().Contains(search) : true) ||
                (f.Planet != null && f.Planet.Name != null ? f.Planet.Name.ToLower().Contains(search) : true) ||
                f.Id.ToString().Contains(search)
            );
        }
        
        if (string.IsNullOrEmpty(orderBy)) return query.ToListAsync();
        {
            var orderQueryBuilder = new StringBuilder();
            var orderParams = orderBy.Trim().Split(',');
            foreach (var param in orderParams.Where(p => !string.IsNullOrWhiteSpace(p)))
            {
                var propertyFromQueryName = param.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name
                    .Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.EndsWith(" desc") ? "DESC" : "ASC";
                orderQueryBuilder.Append($"{objectProperty.Name} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            query = query.OrderBy(orderQuery);
        }

        return query.ToListAsync();
    }

    public IRepository<People> Repository()
    {
        return _peopleRepository;
    }

    public async Task<People> AddAsync(People entity)
    {
        return await _peopleRepository.AddAsync(entity);
    }

    public async Task Clear()
    {
        await _peopleRepository.Clear();
    }
}