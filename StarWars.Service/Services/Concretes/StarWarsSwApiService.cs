using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StarWars.Domain.DTOs;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class StarWarsSwApiService : IStarWarsSwApiService
{
    private readonly HttpClient _starWarsSwApiHttpClient;

    public StarWarsSwApiService(HttpClient starWarsSwApiHttpClient, IConfiguration configuration)
    {
        _starWarsSwApiHttpClient = starWarsSwApiHttpClient;
        if (_starWarsSwApiHttpClient.BaseAddress == null)
            _starWarsSwApiHttpClient.BaseAddress = new Uri(configuration["ApiUrls:StarWarsSwApiUrl"]);
    }

    public async Task<ResponseSwApiDto<T>> DoRequestMany<T>(string subUrl, int page) where T : new()
    {
        Console.WriteLine($"Getting data from: {_starWarsSwApiHttpClient.BaseAddress}{subUrl}?page={page}");
        var response = await _starWarsSwApiHttpClient
            .GetAsync($"{subUrl}?page={page}");
        if (!response.IsSuccessStatusCode) return new ResponseSwApiDto<T>();

        var content = await response.Content.ReadAsStringAsync();

        if (string.IsNullOrEmpty(content)) return new ResponseSwApiDto<T>();

        var peopleResponses = JsonConvert.DeserializeObject<ResponseSwApiDto<T>>(content);

        return peopleResponses.Results != null && !peopleResponses.Results.Any()
            ? new ResponseSwApiDto<T>()
            : peopleResponses;
    }

    public async Task<T> DoSingleRequest<T>(string subUrl) where T : new()
    {
        Console.WriteLine($"Getting data from: {_starWarsSwApiHttpClient.BaseAddress}{subUrl}");
        var response = await _starWarsSwApiHttpClient
            .GetAsync($"{subUrl}");
        if (!response.IsSuccessStatusCode) return new T();

        var content = await response.Content.ReadAsStringAsync();

        return string.IsNullOrEmpty(content) ? new T() : JsonConvert.DeserializeObject<T>(content);
    }
}