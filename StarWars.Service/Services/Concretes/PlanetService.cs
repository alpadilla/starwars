using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class PlanetService : IPlanetService
{
    private readonly IRepository<Planet> _planetRepository;

    public PlanetService(IRepository<Planet> planetRepository)
    {
        _planetRepository = planetRepository;
    }

    public IRepository<Planet> Repository()
    {
        return _planetRepository;
    }

    public async Task<Planet> AddAsync(Planet entity)
    {
        return await _planetRepository.AddAsync(entity);
    }

    public async Task Clear()
    {
        await _planetRepository.Clear();
    }
}