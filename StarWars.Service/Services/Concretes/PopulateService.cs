using AutoMapper;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class PopulateService : IPopulateService
{
    private readonly IMapper _mapper;
    private readonly IStarWarsSwApiService _starWarsSwApiService;

    public PopulateService(IStarWarsSwApiService starWarsSwApiService, IMapper mapper)
    {
        _starWarsSwApiService = starWarsSwApiService;
        _mapper = mapper;
    }

    public async Task<TX> PopulateOne<T, TX>(string subUrl, long id) where T : new()
    {
        var dtoInfo = await _starWarsSwApiService.DoSingleRequest<T>($"{subUrl}/{id}");
        return _mapper.Map<T, TX>(dtoInfo);
    }

    public async Task<List<TX>> PopulateMany<T, TX>(IEnumerable<string>? urls) where T : new()
    {
        var people = new List<TX>();
        foreach (var urlParts in urls?.Select(url => url.Split('/'))!)
            people.Add(await PopulateOne<T, TX>(urlParts[4], Convert.ToInt64(urlParts[5])));
        return people;
    }
}