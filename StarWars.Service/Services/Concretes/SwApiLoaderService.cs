using AutoMapper;
using StarWars.Data.Entities;
using StarWars.Domain.DTOs;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class SwApiLoaderService : ISwApiLoaderService
{
    private readonly IMapper _mapper;
    private readonly IServiceFacade _serviceFacade;

    public SwApiLoaderService(IMapper mapper, IServiceFacade serviceFacade)
    {
        _mapper = mapper;
        _serviceFacade = serviceFacade;
    }

    public async Task<(List<T>, List<TX>)> GetAll<T, TX>(string urlPart) where TX : new() where T : new()
    {
        var entities = new List<TX>();
        var dtos = new List<T>();

        var responseDto = await _serviceFacade.StarWarsSwApiService.DoRequestMany<T>(urlPart, 1);

        if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any()) return (dtos, entities);

        entities.AddRange(responseDto.Results.Select(element =>
        {
            dtos.Add(element);
            return _mapper.Map<T, TX>(element);
        }));

        var result = (responseDto.Count != null ? (decimal)responseDto.Count : 0) / 10;
        var totalOfPages = (int)Math.Ceiling(result);
        if (result % 1 != 0) totalOfPages++;
        for (var i = 2; i <= totalOfPages; i++)
        {
            responseDto = await _serviceFacade.StarWarsSwApiService.DoRequestMany<T>(urlPart, i);

            if (responseDto == null || responseDto.Results == null || !responseDto.Results.Any())
                return (dtos, entities);

            entities.AddRange(responseDto.Results.Select(element =>
            {
                dtos.Add(element);
                return _mapper.Map<T, TX>(element);
            }));
        }

        return (dtos, entities);
    }

    public async Task Load()
    {
        await LoadFilms();
        await LoadPeopleAndSyncWithFilms();
        await LoadPlanetAndSyncWithFilmsAndResidents();
        await LoadSpecieAndSyncWithFilmsAndPeople();
        await LoadStarshipAndSyncWithFilmsAndPilots();
        await LoadVehicleAndSyncWithFilmsAndPilots();
    }

    private async Task LoadFilms()
    {
        await _serviceFacade.FilmService.Clear();
        var (_, films) = await GetAll<FilmDto, Film>("films");
        foreach (var film in films) await _serviceFacade.FilmService.AddAsync(film);
    }

    private async Task LoadPeopleAndSyncWithFilms()
    {
        await _serviceFacade.PeopleService.Clear();
        var (peopleDtos, peoples) = await GetAll<PeopleDto, People>("people");
        foreach (var (people, i) in peoples.Select((p, i) => (p, i)))
        {
            await _serviceFacade.PeopleService.AddAsync(people);
            var transaction = await _serviceFacade.Context.Database.BeginTransactionAsync();
            foreach (var filmUrl in peopleDtos[i].Films!)
                try
                {
                    var filmId = Convert.ToInt64(filmUrl.Split('/')[5]);
                    if (_serviceFacade.Context.Film == null) continue;
                    var film = _serviceFacade.Context.Film.FirstOrDefault(f => f.Id == filmId);
                    if (film != null) people.Films.Add(film);
                }
                catch (Exception)
                {
                    // ignored
                }

            try
            {
                await _serviceFacade.Context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
            }
        }
    }

    private async Task LoadPlanetAndSyncWithFilmsAndResidents()
    {
        await _serviceFacade.PlanetService.Clear();
        var (planetDtos, planets) = await GetAll<PlanetDto, Planet>("planets");
        foreach (var (planet, i) in planets.Select((p, i) => (p, i)))
        {
            await _serviceFacade.PlanetService.AddAsync(planet);
            var transaction = await _serviceFacade.Context.Database.BeginTransactionAsync();
            foreach (var filmUrl in planetDtos[i].Films!)
                try
                {
                    var filmId = Convert.ToInt64(filmUrl.Split('/')[5]);
                    if (_serviceFacade.Context.Film == null) continue;
                    var film = _serviceFacade.Context.Film.FirstOrDefault(f => f.Id == filmId);
                    if (film != null) planet.Films.Add(film);
                }
                catch (Exception)
                {
                    // ignored
                }

            foreach (var residentUrl in planetDtos[i].Residents!)
                try
                {
                    var residentId = Convert.ToInt64(residentUrl.Split('/')[5]);
                    if (_serviceFacade.Context.People == null) continue;
                    var resident = _serviceFacade.Context.People.FirstOrDefault(p => p.Id == residentId);
                    if (resident != null) planet.Residents.Add(resident);
                }
                catch (Exception)
                {
                    // ignored
                }

            try
            {
                await _serviceFacade.Context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
            }
        }
    }

    private async Task LoadSpecieAndSyncWithFilmsAndPeople()
    {
        await _serviceFacade.SpecieService.Clear();
        var (specieDtos, species) = await GetAll<SpecieDto, Specie>("species");
        foreach (var (specie, i) in species.Select((s, i) => (s, i)))
        {
            await _serviceFacade.SpecieService.AddAsync(specie);
            var transaction = await _serviceFacade.Context.Database.BeginTransactionAsync();
            foreach (var filmUrl in specieDtos[i].Films!)
                try
                {
                    var filmId = Convert.ToInt64(filmUrl.Split('/')[5]);
                    if (_serviceFacade.Context.Film == null) continue;
                    var film = _serviceFacade.Context.Film.FirstOrDefault(f => f.Id == filmId);
                    if (film != null) specie.Films.Add(film);
                }
                catch (Exception)
                {
                    // ignored
                }

            foreach (var peopleUrl in specieDtos[i].People!)
                try
                {
                    var peopleId = Convert.ToInt64(peopleUrl.Split('/')[5]);
                    if (_serviceFacade.Context.People == null) continue;
                    var people = _serviceFacade.Context.People.FirstOrDefault(p => p.Id == peopleId);
                    if (people != null) specie.People.Add(people);
                }
                catch (Exception)
                {
                    // ignored
                }

            try
            {
                await _serviceFacade.Context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
            }
        }
    }

    private async Task LoadStarshipAndSyncWithFilmsAndPilots()
    {
        await _serviceFacade.StarshipService.Clear();
        var (starshipDtos, starships) = await GetAll<StarshipDto, Starship>("starships");
        foreach (var (starship, i) in starships.Select((s, i) => (s, i)))
        {
            await _serviceFacade.StarshipService.AddAsync(starship);
            var transaction = await _serviceFacade.Context.Database.BeginTransactionAsync();
            foreach (var starshipUrl in starshipDtos[i].Films!)
                try
                {
                    var filmId = Convert.ToInt64(starshipUrl.Split('/')[5]);
                    if (_serviceFacade.Context.Film == null) continue;
                    var film = _serviceFacade.Context.Film.FirstOrDefault(f => f.Id == filmId);
                    if (film != null) starship.Films.Add(film);
                }
                catch (Exception)
                {
                    // ignored
                }

            foreach (var pilotUrl in starshipDtos[i].Pilots!)
                try
                {
                    var pilotId = Convert.ToInt64(pilotUrl.Split('/')[5]);
                    if (_serviceFacade.Context.People == null) continue;
                    var pilot = _serviceFacade.Context.People.FirstOrDefault(p => p.Id == pilotId);
                    if (pilot != null) starship.Pilots.Add(pilot);
                }
                catch (Exception)
                {
                    // ignored
                }

            try
            {
                await _serviceFacade.Context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
            }
        }
    }

    private async Task LoadVehicleAndSyncWithFilmsAndPilots()
    {
        await _serviceFacade.VehicleService.Clear();
        var (vehicleDtos, vehicles) = await GetAll<VehicleDto, Vehicle>("vehicles");
        foreach (var (vehicle, i) in vehicles.Select((v, i) => (v, i)))
        {
            await _serviceFacade.VehicleService.AddAsync(vehicle);
            var transaction = await _serviceFacade.Context.Database.BeginTransactionAsync();
            foreach (var starshipUrl in vehicleDtos[i].Films!)
                try
                {
                    var filmId = Convert.ToInt64(starshipUrl.Split('/')[5]);
                    if (_serviceFacade.Context.Film == null) continue;
                    var film = _serviceFacade.Context.Film.FirstOrDefault(f => f.Id == filmId);
                    if (film != null) vehicle.Films.Add(film);
                }
                catch (Exception)
                {
                    // ignored
                }

            foreach (var pilotUrl in vehicleDtos[i].Pilots!)
                try
                {
                    var pilotId = Convert.ToInt64(pilotUrl.Split('/')[5]);
                    if (_serviceFacade.Context.People == null) continue;
                    var pilot = _serviceFacade.Context.People.FirstOrDefault(p => p.Id == pilotId);
                    if (pilot != null) vehicle.Pilots.Add(pilot);
                }
                catch (Exception)
                {
                    // ignored
                }

            try
            {
                await _serviceFacade.Context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
            }
        }
    }
}