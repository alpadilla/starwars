using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class VehicleService : IVehicleService
{
    private readonly IRepository<Vehicle> _vehicleRepository;

    public VehicleService(IRepository<Vehicle> vehicleRepository)
    {
        _vehicleRepository = vehicleRepository;
    }

    public IRepository<Vehicle> Repository()
    {
        return _vehicleRepository;
    }

    public async Task<Vehicle> AddAsync(Vehicle entity)
    {
        return await _vehicleRepository.AddAsync(entity);
    }

    public async Task Clear()
    {
        await _vehicleRepository.Clear();
    }
}