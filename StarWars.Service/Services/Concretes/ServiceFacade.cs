using StarWars.Data.DataContext;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class ServiceFacade : IServiceFacade
{
    public ServiceFacade(IFilmService filmService, IPeopleService peopleService,
        IStarWarsSwApiService starWarsSwApiService, IPlanetService planetService,
        ISpecieService specieService, IStarshipService starshipService, IVehicleService vehicleService)
    {
        FilmService = filmService;
        Context = FilmService.Repository().Context();
        PeopleService = peopleService;
        StarWarsSwApiService = starWarsSwApiService;
        PlanetService = planetService;
        SpecieService = specieService;
        StarshipService = starshipService;
        VehicleService = vehicleService;
    }

    public StarWarsDataContext Context { get; }
    public IFilmService FilmService { get; }
    public IPeopleService PeopleService { get; }
    public IPlanetService PlanetService { get; }
    public ISpecieService SpecieService { get; }
    public IStarshipService StarshipService { get; }
    public IVehicleService VehicleService { get; }
    public IStarWarsSwApiService StarWarsSwApiService { get; }
}