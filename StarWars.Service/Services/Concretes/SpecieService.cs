using StarWars.Data.Entities;
using StarWars.Data.Repositories;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Services;

public class SpecieService : ISpecieService
{
    private readonly IRepository<Specie> _specieRepository;

    public SpecieService(IRepository<Specie> specieRepository)
    {
        _specieRepository = specieRepository;
    }

    public IRepository<Specie> Repository()
    {
        return _specieRepository;
    }

    public async Task<Specie> AddAsync(Specie entity)
    {
        return await _specieRepository.AddAsync(entity);
    }

    public async Task Clear()
    {
        await _specieRepository.Clear();
    }
}