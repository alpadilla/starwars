using StarWars.Domain.DTOs;

namespace StarWars.Service.Services.Interfaces;

public interface IStarWarsSwApiService
{
    Task<ResponseSwApiDto<T>> DoRequestMany<T>(string subUrl, int page) where T : new();
    Task<T> DoSingleRequest<T>(string subUrl) where T : new();
}