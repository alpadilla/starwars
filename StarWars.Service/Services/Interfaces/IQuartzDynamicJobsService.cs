using Quartz;

namespace StarWars.Service.Services.Interfaces
{
    public interface IQuartzDynamicJobsService
    {
        Task AddJobForDateAndTrigger<T>(DateTime date, string cronExpression, TimeZoneInfo? timeZoneInfo) where T : IJob;
    }
}