using StarWars.Data.Entities;

namespace StarWars.Service.Services.Interfaces;

public interface IPeopleService : IService<People>
{
    Task<List<People>> GetFromSwApi(string? search, string? populate, string? orderBy);
    Task<List<People>> Get(string? search, string? populate, string? orderBy);
}