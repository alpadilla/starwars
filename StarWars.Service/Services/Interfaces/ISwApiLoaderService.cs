namespace StarWars.Service.Services.Interfaces;

public interface ISwApiLoaderService
{
    Task<(List<T>, List<TX>)> GetAll<T, TX>(string urlPart) where TX : new() where T : new();
    Task Load();
}