using StarWars.Data.DataContext;

namespace StarWars.Service.Services.Interfaces;

public interface IServiceFacade
{
    public StarWarsDataContext Context { get; }
    IFilmService FilmService { get; }
    IPeopleService PeopleService { get; }
    IPlanetService PlanetService { get; }
    ISpecieService SpecieService { get; }
    IStarshipService StarshipService { get; }
    IVehicleService VehicleService { get; }
    IStarWarsSwApiService StarWarsSwApiService { get; }
}