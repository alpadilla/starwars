using StarWars.Data.Entities;

namespace StarWars.Service.Services.Interfaces;

public interface IFilmService : IService<Film>
{
    Task<List<Film>> GetFromSwApi(string? search, string? populate, string? orderBy);
    Task<List<Film>> Get(string? searchExp, string? populate, string? orderBy);
}