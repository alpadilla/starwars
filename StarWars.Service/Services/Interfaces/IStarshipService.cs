using StarWars.Data.Entities;

namespace StarWars.Service.Services.Interfaces;

public interface IStarshipService : IService<Starship>
{
}