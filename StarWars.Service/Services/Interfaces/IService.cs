using StarWars.Data.Entities.Base;
using StarWars.Data.Repositories;

namespace StarWars.Service.Services.Interfaces;

public interface IService<T> where T : EntityBase, new()
{
    IRepository<T> Repository();
    Task<T> AddAsync(T entity);
    Task Clear();
}