namespace StarWars.Service.Services.Interfaces;

public interface IPopulateService
{
    Task<TX> PopulateOne<T, TX>(string subUrl, long id) where T : new();
    Task<List<TX>> PopulateMany<T, TX>(IEnumerable<string>? urls) where T : new();
}