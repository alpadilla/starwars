using StarWars.Data.Entities;

namespace StarWars.Service.Services.Interfaces;

public interface IVehicleService : IService<Vehicle>
{
}