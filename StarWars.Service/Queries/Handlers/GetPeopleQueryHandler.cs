using System.Text;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StarWars.Data.Entities;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Queries.Handlers;

public class GetPeopleQueryHandler : IRequestHandler<GetPeopleQuery, List<People>>
{
    private readonly IServiceFacade _serviceFacade;
    private readonly IDistributedCache _distributedCache;

    public GetPeopleQueryHandler(IServiceFacade serviceFacade, IDistributedCache distributedCache)
    {
        _serviceFacade = serviceFacade;
        _distributedCache = distributedCache;
    }

    public async Task<List<People>> Handle(GetPeopleQuery request, CancellationToken cancellationToken)
    {
        var redisPeople = await _distributedCache.GetAsync(request.CacheKey, cancellationToken);
        if (redisPeople != null)
            return JsonConvert.DeserializeObject<List<People>>(Encoding.UTF8.GetString(redisPeople));
        
        List<People> people;
        try
        {
            people = await _serviceFacade.PeopleService.GetFromSwApi(request.SearchExp, request.Populate, request.OrderBy);
        }
        catch
        {
            people = await _serviceFacade.PeopleService.Get(request.SearchExp, request.Populate, request.OrderBy);
        }

        var serializedPeople = JsonConvert.SerializeObject(people, Formatting.None,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }
        );
        redisPeople = Encoding.UTF8.GetBytes(serializedPeople);
        var options = new DistributedCacheEntryOptions()
            .SetAbsoluteExpiration(DateTime.Now.AddMinutes(10))
            .SetSlidingExpiration(TimeSpan.FromMinutes(2));
        await _distributedCache.SetAsync(request.CacheKey, redisPeople, options, cancellationToken);
        return people;
    }
}