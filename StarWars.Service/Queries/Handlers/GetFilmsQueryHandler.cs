using System.Text;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StarWars.Data.Entities;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Service.Queries.Handlers;

public class GetFilmsQueryHandler : IRequestHandler<GetFilmsQuery, List<Film>>
{
    private readonly IServiceFacade _serviceFacade;
    private readonly IDistributedCache _distributedCache;

    public GetFilmsQueryHandler(IServiceFacade serviceFacade, IDistributedCache distributedCache)
    {
        _serviceFacade = serviceFacade;
        _distributedCache = distributedCache;
    }

    public async Task<List<Film>> Handle(GetFilmsQuery request, CancellationToken cancellationToken)
    {
        var redisFilms = await _distributedCache.GetAsync(request.CacheKey, cancellationToken);
        if (redisFilms != null)
            return JsonConvert.DeserializeObject<List<Film>>(Encoding.UTF8.GetString(redisFilms));
        
        List<Film> films;
        try
        {
            films = await _serviceFacade.FilmService.GetFromSwApi(request.SearchExp, request.Populate, request.OrderBy);
        }
        catch
        {
            films = await _serviceFacade.FilmService.Get(request.SearchExp, request.Populate, request.OrderBy);
        }
            
        var serializedFilms = JsonConvert.SerializeObject(films, Formatting.None,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }
        );
        redisFilms = Encoding.UTF8.GetBytes(serializedFilms);
        var options = new DistributedCacheEntryOptions()
            .SetAbsoluteExpiration(DateTime.Now.AddMinutes(10))
            .SetSlidingExpiration(TimeSpan.FromMinutes(2));
        await _distributedCache.SetAsync(request.CacheKey, redisFilms, options, cancellationToken);
        return films;
    }
}