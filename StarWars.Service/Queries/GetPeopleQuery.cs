using MediatR;
using StarWars.Data.Entities;

namespace StarWars.Service.Queries;

public class GetPeopleQuery : IRequest<List<People>>
{
    public string? CacheKey { get; set; }
    public string? SearchExp { get; set; }
    public string? Populate { get; set; }
    public string? OrderBy { get; set; }
}