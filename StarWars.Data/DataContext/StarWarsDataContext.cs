using Microsoft.EntityFrameworkCore;
using StarWars.Data.Entities;

namespace StarWars.Data.DataContext;

public class StarWarsDataContext : DbContext
{
    public StarWarsDataContext(DbContextOptions<StarWarsDataContext> options)
        : base(options)
    {
    }

    public DbSet<Film>? Film { get; set; }
    public DbSet<People>? People { get; set; }
    public DbSet<Planet>? Planet { get; set; }
    public DbSet<Specie>? Specie { get; set; }
    public DbSet<Starship>? Starship { get; set; }
    public DbSet<Vehicle>? Vehicle { get; set; }
}