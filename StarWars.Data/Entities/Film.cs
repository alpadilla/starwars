using StarWars.Data.Entities.Base;

namespace StarWars.Data.Entities;

public class Film : EntityBase
{
    public Film()
    {
        Characters = new HashSet<People>();
        Planets = new HashSet<Planet>();
        Starships = new HashSet<Starship>();
        Vehicles = new HashSet<Vehicle>();
        Species = new HashSet<Specie>();
    }

    public string? Title { get; set; }
    public string? EpisodeId { get; set; }
    public string? OpeningCrawl { get; set; }
    public string? Director { get; set; }
    public string? Producer { get; set; }
    public DateTime ReleaseDate { get; set; }
    public ICollection<People> Characters { get; set; }
    public ICollection<Planet> Planets { get; set; }
    public ICollection<Starship> Starships { get; set; }
    public ICollection<Vehicle> Vehicles { get; set; }
    public ICollection<Specie> Species { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}