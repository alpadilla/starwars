namespace StarWars.Data.Entities.Base;

public abstract class EntityBase
{
    public long Id { get; set; }
}