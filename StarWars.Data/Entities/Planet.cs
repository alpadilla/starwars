using StarWars.Data.Entities.Base;

namespace StarWars.Data.Entities;

public class Planet : EntityBase
{
    public Planet()
    {
        Films = new HashSet<Film>();
        Residents = new List<People>();
    }

    public string? Name { get; set; }
    public string? RotationPeriod { get; set; }
    public string? OrbitalPeriod { get; set; }
    public string? Diameter { get; set; }
    public string? Climate { get; set; }
    public string? Cravity { get; set; }
    public string? Terrain { get; set; }
    public string? SurfaceWater { get; set; }
    public string? Population { get; set; }
    public ICollection<People> Residents { get; set; }
    public ICollection<Film> Films { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}