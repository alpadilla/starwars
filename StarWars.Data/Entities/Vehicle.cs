using StarWars.Data.Entities.Base;

namespace StarWars.Data.Entities;

public class Vehicle : EntityBase
{
    public Vehicle()
    {
        Films = new HashSet<Film>();
        Pilots = new HashSet<People>();
    }

    public string? CostInCredits { get; set; }
    public string? Length { get; set; }
    public string? MaxAtmospheringSpeed { get; set; }
    public string? Crew { get; set; }
    public string? Passengers { get; set; }
    public string? CargoCapacity { get; set; }
    public string? Consumables { get; set; }
    public string? HyperdriveRating { get; set; }
    public string? Mglt { get; set; }
    public string? VehicleClass { get; set; }
    public ICollection<People> Pilots { get; set; }
    public ICollection<Film> Films { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}