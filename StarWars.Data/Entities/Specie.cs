using StarWars.Data.Entities.Base;

namespace StarWars.Data.Entities;

public class Specie : EntityBase
{
    public Specie()
    {
        Films = new HashSet<Film>();
        People = new HashSet<People>();
    }

    public string? Name { get; set; }
    public string? Classification { get; set; }
    public string? Designation { get; set; }
    public string? AverageHeight { get; set; }
    public string? SkinColors { get; set; }
    public string? HairColors { get; set; }
    public string? EyeColors { get; set; }
    public string? AverageLifespan { get; set; }
    public string? Homeworld { get; set; }
    public string? Language { get; set; }
    public ICollection<People> People { get; set; }
    public ICollection<Film> Films { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}