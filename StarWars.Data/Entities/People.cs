using StarWars.Data.Entities.Base;

namespace StarWars.Data.Entities;

public class People : EntityBase
{
    public People()
    {
        Films = new HashSet<Film>();
        Species = new HashSet<Specie>();
        Vehicles = new HashSet<Vehicle>();
        Starships = new HashSet<Starship>();
    }

    public string? Name { get; set; }
    public string? Height { get; set; } // Integer
    public string? Mass { get; set; } // Integer
    public string? HairColor { get; set; }
    public string? SkinColor { get; set; }
    public string? EyeColor { get; set; }
    public string? BirthYear { get; set; }
    public string? Gender { get; set; }
    public string? Homeworld { get; set; }
    public ICollection<Film> Films { get; set; }
    public ICollection<Specie> Species { get; set; }
    public ICollection<Vehicle> Vehicles { get; set; }
    public ICollection<Starship> Starships { get; set; }
    public Planet? Planet { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}