using Microsoft.EntityFrameworkCore;
using StarWars.Data.DataContext;
using StarWars.Data.Entities.Base;

namespace StarWars.Data.Repositories;

public class Repository<T> : IRepository<T> where T : EntityBase, new()
{
    private readonly StarWarsDataContext _starWarsDataContext;

    public Repository(StarWarsDataContext starWarsDataContext)
    {
        _starWarsDataContext = starWarsDataContext;
    }

    public StarWarsDataContext Context()
    {
        return _starWarsDataContext;
    }

    public DbSet<T> Query()
    {
        return _starWarsDataContext.Set<T>();
    }

    public IQueryable<T> GetAll()
    {
        try
        {
            return _starWarsDataContext.Set<T>();
        }
        catch (Exception ex)
        {
            throw new Exception($"Couldn't retrieve entities: {ex.Message}");
        }
    }

    public async Task<T> GetByIdAsync(long id)
    {
        try
        {
            var entity = await _starWarsDataContext.Set<T>().FindAsync(id);
            if (entity == null)
                throw new KeyNotFoundException($"Entity {nameof(T)} was not found.");
            _starWarsDataContext.Entry(entity).State = EntityState.Detached;
            return entity;
        }
        catch (Exception)
        {
            throw new Exception($"{typeof(T).Name} could not retrieved");
        }
    }

    public async Task<T> AddAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException($"{nameof(entity)} should not be null");
        var transaction = await _starWarsDataContext.Database.BeginTransactionAsync();
        try
        {
            if (entity.Id > 0)
                await _starWarsDataContext.Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT dbo.{typeof(T).Name} ON");
            await _starWarsDataContext.AddAsync(entity);
            await _starWarsDataContext.SaveChangesAsync();
            if (entity.Id > 0)
                await _starWarsDataContext.Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT dbo.{typeof(T).Name} OFF");
            await transaction.CommitAsync();
            return entity;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            await transaction.RollbackAsync();
            throw new Exception($"{typeof(T).Name} could not be saved");
        }
    }

    public async Task<T> UpdateAsync(T entity, long id)
    {
        try
        {
            var ent = await GetByIdAsync(id);
            entity.Id = ent.Id;
            _starWarsDataContext.Update(entity);
            await _starWarsDataContext.SaveChangesAsync();
            return entity;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            throw new Exception($"{typeof(T).Name} could not be updated");
        }
    }

    public async Task DeleteAsync(long id)
    {
        try
        {
            var entity = _starWarsDataContext.Set<T>().FirstOrDefault(entity => entity.Id == id);
            if (entity == null) throw new KeyNotFoundException($"{nameof(T)} could not be deleted, id not found");
            _starWarsDataContext.Remove(entity);
            await _starWarsDataContext.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw new Exception($"{typeof(T).Name} could not be deleted");
        }
    }

    public async Task Clear()
    {
        await _starWarsDataContext.Database.ExecuteSqlRawAsync($"DELETE FROM dbo.{typeof(T).Name}");
    }
}