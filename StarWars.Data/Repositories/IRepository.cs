using Microsoft.EntityFrameworkCore;
using StarWars.Data.DataContext;

namespace StarWars.Data.Repositories;

public interface IRepository<T> where T : class, new()
{
    StarWarsDataContext Context();
    DbSet<T> Query();
    IQueryable<T> GetAll();
    Task<T> GetByIdAsync(long id);
    Task<T> AddAsync(T entity);
    Task<T> UpdateAsync(T entity, long id);
    Task DeleteAsync(long id);
    Task Clear();
}