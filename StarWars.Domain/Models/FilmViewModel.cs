namespace StarWars.Domain.Models;

public class FilmViewModel
{
    public long Id { get; set; }
    public string? Title { get; set; }
    public string? Director { get; set; }
    public string? Producer { get; set; }
    public string? Characters { get; set; }
    public string? ReleaseDate { get; set; }
}