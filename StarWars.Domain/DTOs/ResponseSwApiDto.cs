namespace StarWars.Domain.DTOs;

public class ResponseSwApiDto<T> where T : new()
{
    public int? Count { get; set; }
    public string? Next { get; set; }
    public string? Previous { get; set; }
    public List<T>? Results { get; set; }
}