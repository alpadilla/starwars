using Newtonsoft.Json;

namespace StarWars.Domain.DTOs;

public class VehicleDto
{
    [JsonProperty(PropertyName = "cost_in_credits")]
    public string? CostInCredits { get; set; }

    public string? Length { get; set; }

    [JsonProperty(PropertyName = "max_atmosphering_speed")]
    public string? MaxAtmospheringSpeed { get; set; }

    public string? Crew { get; set; }
    public string? Passengers { get; set; }

    [JsonProperty(PropertyName = "cargo_capacity")]
    public string? CargoCapacity { get; set; }

    public string? Consumables { get; set; }

    [JsonProperty(PropertyName = "hyperdrive_rating")]
    public string? HyperdriveRating { get; set; }

    [JsonProperty(PropertyName = "MGLT")] public string? Mglt { get; set; }

    [JsonProperty(PropertyName = "vehicle_class")]
    public string? VehicleClass { get; set; }

    public List<string>? Pilots { get; set; }
    public List<string>? Films { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}