namespace StarWars.Domain.DTOs;

public class ResponseDto<T> where T : class
{
    public T? Payload { get; set; }
    public List<string> Errors { get; set; }
    
    public ResponseDto(T? payload = null)
    {
        Payload = payload;
        Errors = new List<string>();
    }
}