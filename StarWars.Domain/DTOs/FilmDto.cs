using Newtonsoft.Json;

namespace StarWars.Domain.DTOs;

public class FilmDto
{
    public string? Title { get; set; }

    [JsonProperty(PropertyName = "episode_id")]
    public string? EpisodeId { get; set; }

    [JsonProperty(PropertyName = "opening_crawl")]
    public string? OpeningCrawl { get; set; }

    public string? Director { get; set; }
    public string? Producer { get; set; }

    [JsonProperty(PropertyName = "release_date")]
    public DateTime ReleaseDate { get; set; }

    public List<string>? Characters { get; set; }
    public List<string>? Planets { get; set; }
    public List<string>? Starships { get; set; }
    public List<string>? Vehicles { get; set; }
    public List<string>? Species { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}