using Newtonsoft.Json;

namespace StarWars.Domain.DTOs;

public class PeopleDto
{
    public string? Name { get; set; }
    public string? Height { get; set; } // Integer
    public string? Mass { get; set; } // Integer

    [JsonProperty(PropertyName = "hair_color")]
    public string? HairColor { get; set; }

    [JsonProperty(PropertyName = "skin_color")]
    public string? SkinColor { get; set; }

    [JsonProperty(PropertyName = "eye_color")]
    public string? EyeColor { get; set; }

    [JsonProperty(PropertyName = "birth_year")]
    public string? BirthYear { get; set; }

    public string? Gender { get; set; }
    public string? Homeworld { get; set; }
    public List<string>? Films { get; set; }
    public List<string>? Species { get; set; }
    public List<string>? Vehicles { get; set; }
    public List<string>? Starships { get; set; }
    public DateTime Created { get; set; }
    public DateTime Edited { get; set; }
    public string? Url { get; set; }
}