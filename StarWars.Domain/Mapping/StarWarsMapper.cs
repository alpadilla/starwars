using AutoMapper;
using StarWars.Domain.DTOs;
using StarWars.Domain.Models;
using Film = StarWars.Data.Entities.Film;
using People = StarWars.Data.Entities.People;
using Planet = StarWars.Data.Entities.Planet;
using Specie = StarWars.Data.Entities.Specie;
using Starship = StarWars.Data.Entities.Starship;
using Vehicle = StarWars.Data.Entities.Vehicle;

namespace StarWars.Domain.Mapping;

public class StarWarsMapper : Profile
{
    public StarWarsMapper()
    {
        CreateMap<FilmDto, Film>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            ).ForMember(dest => dest.Characters,
                opt =>
                    opt.MapFrom(src => new List<People>())
            ).ForMember(dest => dest.Planets,
                opt =>
                    opt.MapFrom(src => new List<Planet>())
            ).ForMember(dest => dest.Starships,
                opt =>
                    opt.MapFrom(src => new List<Starship>())
            ).ForMember(dest => dest.Vehicles,
                opt =>
                    opt.MapFrom(src => new List<Vehicle>())
            ).ForMember(dest => dest.Species,
                opt =>
                    opt.MapFrom(src => new List<Specie>())
            );
        CreateMap<PeopleDto, People>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            )
            .ForMember(dest => dest.Films,
                opt =>
                    opt.MapFrom(src => new List<Film>())
            ).ForMember(dest => dest.Starships,
                opt =>
                    opt.MapFrom(src => new List<Starship>())
            ).ForMember(dest => dest.Vehicles,
                opt =>
                    opt.MapFrom(src => new List<Vehicle>())
            ).ForMember(dest => dest.Species,
                opt =>
                    opt.MapFrom(src => new List<Specie>())
            );
        CreateMap<PlanetDto, Planet>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            )
            .ForMember(dest => dest.Residents,
                opt =>
                    opt.MapFrom(src => new List<People>())
            )
            .ForMember(dest => dest.Films,
                opt =>
                    opt.MapFrom(src => new List<Film>())
            );
        CreateMap<SpecieDto, Specie>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            )
            .ForMember(dest => dest.People,
                opt =>
                    opt.MapFrom(src => new List<People>())
            )
            .ForMember(dest => dest.Films,
                opt =>
                    opt.MapFrom(src => new List<Film>())
            );
        CreateMap<StarshipDto, Starship>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            )
            .ForMember(dest => dest.Pilots,
                opt =>
                    opt.MapFrom(src => new List<People>())
            )
            .ForMember(dest => dest.Films,
                opt =>
                    opt.MapFrom(src => new List<Film>())
            );
        CreateMap<VehicleDto, Vehicle>()
            .ForMember(dest => dest.Id,
                opt =>
                    opt.MapFrom(src
                        => Convert.ToInt64(src.Url!.Split('/', StringSplitOptions.None)[5])
                    )
            )
            .ForMember(dest => dest.Pilots,
                opt =>
                    opt.MapFrom(src => new List<People>())
            )
            .ForMember(dest => dest.Films,
                opt =>
                    opt.MapFrom(src => new List<Film>())
            );

        CreateMap<Film, FilmViewModel>()
            .ForMember(dest => dest.Characters,
                opt =>
                    opt.MapFrom(src => src.Characters.Aggregate("",
                        (a, b) => a != "" ? a + ", " + (b.Name ?? string.Empty) : b.Name ?? string.Empty 
                    ))
            ).ForMember(dest => dest.ReleaseDate,
                opt =>
                    opt.MapFrom(src => $"{src.ReleaseDate:dd-MM-yyyy}" 
                    )
            );
    }
}