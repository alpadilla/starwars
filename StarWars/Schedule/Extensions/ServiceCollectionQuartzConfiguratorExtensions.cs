﻿using Quartz;

namespace StarWars.Schedule.Extensions
{
    public static class ServiceCollectionQuartzConfiguratorExtensions
    {
        /// <summary>
        /// This extension allows registering a Job with Quartz, and setting it's trigger schedule.
        /// </summary>
        public static void AddJobAndTrigger<T>(this IServiceCollectionQuartzConfigurator quartz, IConfiguration config)
        where T : IJob
        {
            var currentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var jobName = typeof(T).Name;
            Console.WriteLine($"[QUARTZ] Adding Job with Name: {jobName} with {currentEnvironment} environment");

            var configKey = $"Quartz:{jobName}";
            var cronSchedule = config[configKey];

            if (string.IsNullOrEmpty(cronSchedule))
            {
                throw new Exception($"No Quartz Cron schedule found for job in configuration at {configKey}");
            }

            var jobKey = new JobKey(jobName);
            quartz.AddJob<T>(opts => opts.WithIdentity(jobKey));

            quartz.AddTrigger(opts => opts
                .ForJob(jobKey)
                .WithIdentity(jobName + "-trigger")
                .WithCronSchedule(cronSchedule)); // use the schedule from configuration
            Console.WriteLine($"[QUARTZ] Job with Name: {jobName} with {currentEnvironment} environment added");
        }
    }
}
