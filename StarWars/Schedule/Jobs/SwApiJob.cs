using System.Globalization;
using Quartz;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Schedule.Jobs;

public class SwApiJob : IJob
{
    private readonly ISwApiLoaderService _swApiLoaderService;
    private readonly IServiceFacade _serviceFacade;
    
    public SwApiJob(ISwApiLoaderService swApiLoaderService, IServiceFacade serviceFacade)
    {
        _swApiLoaderService = swApiLoaderService;
        _serviceFacade = serviceFacade;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        try
        {
            Console.WriteLine("[STARTING] Load entities from SWAPI");
            Console.WriteLine($"[STEP 1] Current Time: {DateTime.Now}");
            Console.WriteLine("[STEP 2] Starting the load data from SWAPI");
            await _swApiLoaderService.Load();
            Console.WriteLine("[FINISHED] SWAPI");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}