using Microsoft.OpenApi.Models;
using StarWars.Data.Entities;
using StarWars.Domain.DTOs;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWars.APIDoc;

public class CustomDocumentFilter : IDocumentFilter
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        context.SchemaGenerator.GenerateSchema(typeof(People), context.SchemaRepository);
        context.SchemaGenerator.GenerateSchema(typeof(ResponseDto<>), context.SchemaRepository);
    }
}