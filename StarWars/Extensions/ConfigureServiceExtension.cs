using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Quartz;
using StarWars.Schedule.Extensions;
using StarWars.APIDoc;
using StarWars.Data.DataContext;
using StarWars.Data.Repositories;
using StarWars.Domain.Mapping;
using StarWars.Schedule.Jobs;
using StarWars.Service.Services;
using StarWars.Service.Services.Concretes;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Extensions;

public static class ConfigureServiceExtension
{
    public static void RegisterDataBaseContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<StarWarsDataContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
    }

    public static void RegisterHttpClients(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpClient("StarWarsSwApi",
            client => { client.BaseAddress = new Uri(configuration["ApiUrls:StarWarsSwApiUrl"]); });

        services.AddSingleton<IStarWarsSwApiService, StarWarsSwApiService>(p =>
        {
            var httpClientFactory = p.GetRequiredService<IHttpClientFactory>();
            var httpClient = httpClientFactory.CreateClient("StarWarsSwApi");
            return new StarWarsSwApiService(httpClient, configuration);
        });
    }

    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddScoped<IPopulateService, PopulateService>();
        services.AddScoped<IStarWarsSwApiService, StarWarsSwApiService>();
        services.AddScoped<IServiceFacade, ServiceFacade>();
        services.AddScoped<IFilmService, FilmService>();
        services.AddScoped<IPeopleService, PeopleService>();
        services.AddScoped<IPlanetService, PlanetService>();
        services.AddScoped<ISpecieService, SpecieService>();
        services.AddScoped<IStarshipService, StarshipService>();
        services.AddScoped<IVehicleService, VehicleService>();
        services.AddScoped<ISwApiLoaderService, SwApiLoaderService>();
    }

    public static void RegisterRepositories(this IServiceCollection services)
    {
        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
    }

    public static void RegisterAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(typeof(StarWarsMapper));
    }
    
    public static void AddSwaggerConfiguration(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1.0.0",
                Title = "Star Wars",
                Description = "API to get information from SWAPI.",
                Contact = new OpenApiContact
                {
                    Name = "Alvaro Luis Padilla Moya",
                    Email = "alpadilla2006@gmail.com"
                }
            });
            
            c.DocumentFilter<CustomDocumentFilter>();
        });

    }

    public static void AddQuartzConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddQuartz(q =>
        {
            q.UseMicrosoftDependencyInjectionJobFactory();
            q.AddJobAndTrigger<SwApiJob>(configuration);
        });
        
        services.AddQuartzServer(options =>
        {
            options.WaitForJobsToComplete = true;
        });
    }
}