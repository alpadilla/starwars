using System.Text.Json.Serialization;
using MediatR;
using StarWars.Extensions;
using StarWars.Service.Queries;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews()
    .AddJsonOptions(options => { options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles; });
;
builder.Services.RegisterDataBaseContext(builder.Configuration);
builder.Services.RegisterHttpClients(builder.Configuration);
builder.Services.RegisterAutoMapper();
builder.Services.RegisterServices();
builder.Services.RegisterRepositories();
builder.Services.AddSwaggerConfiguration();
builder.Services.AddQuartzConfiguration(builder.Configuration);
builder.Services.AddMediatR(typeof(GetFilmsQuery).Assembly);

builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration["Cache:ConnectionString"];
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

// app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    "default",
    "{controller=Home}/{action=Index}/{id?}");

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Star Wars API v1");
    c.RoutePrefix = "api/doc";
});


app.Run();