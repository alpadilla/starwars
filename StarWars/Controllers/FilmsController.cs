using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using StarWars.Domain.Models;
using StarWars.Service.Queries;
using Film = StarWars.Data.Entities.Film;

namespace StarWars.Controllers;

public class FilmsController : Controller
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public FilmsController(IMediator mediator, IMapper mapper)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    public async Task<IActionResult> Index()
    {
        var films = _mapper.Map<List<Film>, List<FilmViewModel>>(
            await _mediator.Send(new GetFilmsQuery
            {
                Populate = null,
                OrderBy = null,
                SearchExp = null,
                CacheKey = $"{Request.Path}{Request.QueryString}"
            })
        );
        return films.Any() ? View(films) : Problem("An error occurred getting the films.");
    }
}