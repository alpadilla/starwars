using MediatR;
using Microsoft.AspNetCore.Mvc;
using StarWars.Service.Queries;
using StarWars.Service.Services.Interfaces;

namespace StarWars.Controllers;

public class PeopleController : Controller
{
    private readonly IMediator _mediator;

    public PeopleController(IMediator mediator)
    {
        _mediator = mediator;
    }

    // GET
    public async Task<IActionResult> Index()
    {
        var people = await _mediator.Send(new GetPeopleQuery
        {
            Populate = null,
            OrderBy = null,
            SearchExp = null,
            CacheKey = $"{Request.Path}{Request.QueryString}"
        });
        return people.Any() ? View(people) : Problem("An error occurred getting the people.");
    }
}