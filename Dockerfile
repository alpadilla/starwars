﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
#EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["StarWars/StarWars.csproj", "StarWars/"]
RUN dotnet restore "StarWars/StarWars.csproj"
COPY . .
WORKDIR "/src/StarWars"
RUN dotnet build "StarWars.csproj" -c Release -o /app/build
#RUN dotnet tool install -g dotnet-ef
#ENV PATH "$PATH:/root/.dotnet/tools/"
#RUN dotnet ef database update --project /src/StarWars.Data/StarWars.Data.csproj --startup-project /src/StarWars


FROM build AS publish
RUN dotnet publish "StarWars.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "StarWars.dll"]
