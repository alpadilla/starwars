# StarWars



## Getting started

This project was created in order to obtain information from the Star Wars universe from the free API https://swapi.dev/.

## Dependencies

This project was made using Redis as a Cache server. Please consider that you must have a Redis DB server running and set the "ConnectionString" in appsettings.json.

## Clone
```
cd my_proyect
git remote add origin https://gitlab.com/alpadilla/starwars.git
git branch -M development
git push -uf origin development
```

## Add migration
```
dotnet ef migrations add InitialStructure --project ./StarWars.Data/StarWars.Data.csproj --startup-project ./StarWars
```

## Remove migration
```
dotnet ef migrations remove --project ./StarWars.Data/StarWars.Data.csproj --startup-project ./StarWars
```

## Database update
```
dotnet ef database update --project ./StarWars.Data/StarWars.Data.csproj --startup-project ./StarWars
```

## Docker

There are two docker-compose files, if you are a Mac OS M1 developer please use docker-compose-m1.yml, in other cases please use the docker-compose.yml.

## Authors
Alvaro Luis Padilla Moya
